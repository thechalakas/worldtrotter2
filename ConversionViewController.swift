//
//  ConversionViewController.swift
//  WorldTrotter
//
//  Created by Jay on 26/07/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit

class ConversionViewController: UIViewController, UITextFieldDelegate
{
    //code comes here
    
    //when app launches, the celsius shows the default value, which is set at 100
    //since this is about app launches thing, lets overload the method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //now that I have multiple controllers, just adding some debug messages to see how things are happening.
        print("ConversionViewController has just been awoken! ")
        
        //lets call the method that udpates the celcius
        //by default, if there is nothing to convert or update, it will show ??? mark anyway
        updateCelsiusLabel()
    }
    
    //to start with, lets make it so that the text box for the degree will
    //simply show what the user types
    
    //outlet (from UI to controller)
    @IBOutlet var celsiusLabel: UILabel!
    
    //this is for storing the entered value as a measurement type
    //question mark means, it may not be entered.
    //like if instead of number, user enters a non-number
    var fahrenheitValue: Measurement<UnitTemperature>?
    {
        didSet //everytime the fahrenheit value changes, this method gets called.
        {
            updateCelsiusLabel()
        }
    }
    
    
    //this is the celsius value being calculated based on the above fahrenheit value.
    //normally, such conversion would be done inside a method, but here, they are plugging the 
    //conversion to the variable declartion itself
    var celsiusValue: Measurement<UnitTemperature>?
    {
        if let fahrenheitValue = fahrenheitValue //here checking if fahrenheit value is indeed a fahrenheit value, and not some non-fahrenheit value
        {
            return fahrenheitValue.converted(to: .celsius)
        }
        else
        {
            return nil //that means, the user has entered some non-fahrenheit number
        }
    }
    
    //now, we need a method that can update the celci us label. 
    //previously, we did this directly in the editing table changed option but now we are changing the implementation
    //this method will push whatever is there in the celsiusValue property
    //remember that celsiusValue dynamically calculates the correct celsius value based on whatever is stored 
    //in fahrenheit
    func updateCelsiusLabel()
    {
        if let celsiusValue = celsiusValue //checking if celsius is indeed celcius and not some null thing.
        {
            //celsiusLabel.text = "\(celsiusValue.value)"
            //i dont know what this \ and bracket thing is. the book does not explain
            
            celsiusLabel.text = numberFormatter.string(from: NSNumber(value: celsiusValue.value))
        }
        else
        {
            let blank_string = "???"  //i am doing this here because, leaving this to become blank would make the box dissapear :(
            celsiusLabel.text = blank_string
        }
    }
    
    //action (from controller to UI)
    @IBAction func fahrenheitFieldEditingChanged(_ textField: UITextField)
    {
        /*
        
         //this is the old implementation where we were simply mimicking the input to output
         
        let text = textField.text //collecting the text from the UI text field
        if(text?.isEmpty)! //checking if it is empty
        {
            let blank_string = "???"  //i am doing this here because, leaving this to become blank would make the box dissapear :(
            celsiusLabel.text = blank_string
        }
        else
        {
            celsiusLabel.text = textField.text
        }
         */
        
        //here, whenever user enters something, we update the fahreinheit property by taking what the user enter
        
        //alright, the new issue we are facing is that, we have enabled the input to work with decimal which looks like a comman in Spain
        //as part of our internationalization/localization efforts
        
        //however, while the input is fine, we are unable to calculate the output because our method cannot handle command entered numbers
        
        
        //commenting out the entire if else here, and replacing it with a new if else that works independent of comma or decimal
        
        /*
        if let text = textField.text, let value = Double(text)  //here, 1st checking if user has actually entered some text. then, 2nd, converting that text into a double number.
        {
            fahrenheitValue = Measurement(value: value, unit: .fahrenheit)  //here converting that double number into a value of type Measurement.
        }
        else
        {
            fahrenheitValue = nil
        }
        */
        
        //new if else that works independent of comma or decimal
        
        let text = textField.text  //get the text
        let number = numberFormatter.number(from: text!) //now extract the number from the text
        
        if((number) != nil)  //if number is for real, then lets return the extracted fahrenheit value from the input
        {
            fahrenheitValue = Measurement(value: (number?.doubleValue)!, unit: .fahrenheit)
            //when compared to the earlier code, we were simply collecting the double version of the string
            //this time though, we are collected the double value from the number property, which gets its text from the number formatter
            //number formattre of course, is region aware so gives us the number although the input has commas in it
        }
        else //else return nil
        {
            fahrenheitValue = nil
        }
    }
    
    //alright, now the next issue is, the keyboard stays up all the time
    //we need to make it so that, tapping on the background makes the keyboard dissapear
    
    //the input text box is what made the keyboard appear
    //so we need to grab on to that text, and then make it so that the keybaord disappears
    
    @IBOutlet var textField: UITextField!  //this will help me connect to the text field. 
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer)  //collect the tap
    {
        //the gesture that will trigger this method will be the one that informs us that the user has tapped the background
        textField.resignFirstResponder()  //we tell the text box to retire the keyboard.
    }
    
    //okay, an issue with the app right now is that, the celsius display goes no for the full precision of the decimal thing
    //to make the display more convenient, we will create a number formatter
    
    let numberFormatter: NumberFormatter =
    {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 1
        return nf
    }()
    
    //okay, now go back up to the celcius display method and use the number formatter
    //done that
    
    //okay, another issue is that I can actually enter a second decimal or a third decimal.
    //obviously, that is not cool.
    
    //the book recommends using a delegate which will allow me to collect multiple event related callbacks
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        /*
        
        print("Current text: \(textField.text)")
        print("Replacement text: \(string)")
        return true
 
        */
        
        
        //let existingTextHasDecimalSeparator = textField.text?.range(of: ".")
        //let replacementTextHasDecimalSeparator = string.range(of: ".")
        
        //alright, we are looking at localization now. 
        //for instance, changing the app or simulator region to spain makes the the dot show up as 
        //comma. that means, most of the code will not work including allowing two commas to appear 
        //lets fix this then
        
        //commenting out the above two assignments of existingTextHasDecimalSeparator & replacementTextHasDecimalSeparator
        //giving them new assignments
        let currentLocale = Locale.current
        let decimalSeparator = currentLocale.decimalSeparator ?? "."

        let existingTextHasDecimalSeparator = textField.text?.range(of: decimalSeparator)
        let replacementTextHasDecimalSeparator = string.range(of: decimalSeparator)
        
        
        if existingTextHasDecimalSeparator != nil,replacementTextHasDecimalSeparator != nil
        {
            return false
        }
        else
        {
            return true
        }
    }
    
}
