//
//  MapViewController.swift
//  WorldTrotter
//
//  Created by Jay on 27/07/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit  //starting UI frameworks
import MapKit //this is for the map views


class MapViewController : UIViewController
{
    //the idea with this set of changes is to create the views programmatically
    
    //first, the type of view we want. in this case, the map view (I just deleted the story board drag and drop view I created before coming here
    var mapView: MKMapView!
    
    //we need override the loadView method to load our view programatically
    override func loadView()
    {
        mapView = MKMapView() //initializing the view
        //attaching this view to the current controller
        view = mapView
        
        //now, its time to add some map option like buttons, and of course add constraints to these buttons, all programatically
        //even as I do this, it is clear that we are essentially using code to design our UI. reminds me of those times when we were building UI in XAML using drag and drop
        //depending on the situation, I should decide to either use the drag and drop system or code like how I am doing here.
        //for the map with its sparse buttons (or any app with sparse UI) using code to build UI is reduntant
        //still, we need to know this, so lets get this done with
        
        //let segmentedControl = UISegmentedControl(items: ["Standard","Hybrid","Satellite"]) // A UI control with three buttons
        
        //Okay, the above strings are used to diplay the buttons on the map.
        //need to internationalize the thing with a string table. so commented out the above segmentedControl strings
        
        //first the three strings in their internationalization version
        let standardString = NSLocalizedString("standard", comment: "standard map view")
        let satelliteString = NSLocalizedString("Satellite", comment: "satellite map view")
        let hybridString = NSLocalizedString("Hybrid", comment: "Hybrid map view")
        
        //bundle all these three into the segmentedControl 
        let segmentedControl = UISegmentedControl(items: [standardString,satelliteString,hybridString])
        
        
        segmentedControl.backgroundColor = UIColor.white.withAlphaComponent(0.5)  //setting the background color
        segmentedControl.selectedSegmentIndex = 0; //choosing the default button that will be highlighted
        
        //iOS had this autoresizing mask feature before auto layout systems were reengineered. 
        //however, this mask feature is still enabled by default. that would clash with our UI things so we will switch it off here
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        
        //alright, that configues our new UI control/view thing. lets add it to the current view.
        view.addSubview(segmentedControl)
        
        //at this point, the UI segment control is visible but it does not have any constraints related to the layout
        //so it will distributed at the top left corner. 
        //lets add some constraints
        
        //let topConstraint = segmentedControl.topAnchor.constraint(equalTo: view.topAnchor)
        let topConstraint = segmentedControl.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 8)
        
        
        //let leadingConstraint = segmentedControl.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        //let trailingConstraint = segmentedControl.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        
        let margins = view.layoutMarginsGuide
        let leadingConstraint = segmentedControl.leadingAnchor.constraint(equalTo: margins.leadingAnchor)
        let trailingConstraint = segmentedControl.trailingAnchor.constraint(equalTo: margins.trailingAnchor)
        
        //I have three anchors, with each of them assigned a constraint. 
        //However, I need to turn them on - by rubbing its feet I guess 
        
        //try mixing up the true and false values below to see how constraints have an effect on a control
        //its kind of fun
        
        topConstraint.isActive = true
        leadingConstraint.isActive = true
        trailingConstraint.isActive = true
        
        //at this point, we have the UI control at the top, streteched from left to right.
        //unfortunately, they are located below the status bar, so the UI stuff is overlapping with the time and battery indicator and all that.
        
        //lets fix that
        //for this, I will comment out the original assignment of the top constraint and add a constant to it, pushing it downwards
        
        //now that the top part is taken care of, lets do the left and side margins
        
        //once again, we will comment out the original constraint assignments for leading and trailing
        //give it
        
        //alright, now there are auto adjusting margins set on all three sides of the UI control
        
        //now, lets make those three taps do something eh?
        

        
        
        //now, lets connect the UI control's three different taps to this method
        
        segmentedControl.addTarget(self, action: #selector(MapViewController.mapTypeChanged(_:)), for: .valueChanged)
        
    }
    
    //first the method that will react to the trigger
    @objc func mapTypeChanged(_ segControl: UISegmentedControl)
    {
        switch segControl.selectedSegmentIndex
        {
            case 0: mapView.mapType = .standard
            case 1: mapView.mapType = .hybrid
            case 2: mapView.mapType = .satellite
            default: break
        }
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //now that I have multiple controllers, just adding some debug messages to see how things are happening.
        print("MapViewController has just been awoken! ")	
        
    }
}
